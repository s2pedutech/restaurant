import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RestosignupComponent } from './restosignup/restosignup.component';
import { CustomersignupComponent } from './customersignup/customersignup.component';
import {RouterModule,Routes} from '@angular/router';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { CustloginComponent } from './custlogin/custlogin.component';
import { StorageServiceModule } from 'ngx-webstorage-service';
var routes:Routes=[{path:'',redirectTo:'home',pathMatch:'full'},
  {path:'home',component:HomeComponent},
  {path:'login',component:LoginComponent},
  {path:'login/customer',component:CustloginComponent},
  {path:'signup/restro',component:RestosignupComponent},
  {path:'signup/customer',component:CustomersignupComponent},
  {path:'admin',loadChildren:'./admin/admin.module#AdminModule'},
  {path:'restro',loadChildren:'./restorant/restorant.module#RestorantModule'},
  {path:'customer',loadChildren:'./customer/customer.module#CustomerModule'}
]

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    RestosignupComponent,
    CustomersignupComponent,
    CustloginComponent
  ],
  imports: [
    BrowserModule,HttpClientModule,
    AppRoutingModule,RouterModule.forRoot(routes),
    FormsModule,ReactiveFormsModule,StorageServiceModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
