import { Component, OnInit } from '@angular/core';
import {FormGroup,FormControl,Validators} from '@angular/forms';
import {CustomersignupService} from '../customersignup.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-customersignup',
  templateUrl: './customersignup.component.html',
  styleUrls: ['./customersignup.component.css']
})
export class CustomersignupComponent implements OnInit {

  custform = new FormGroup({
    id: new FormControl(),
    name: new FormControl('', Validators.required),
    email: new FormControl('',[Validators.required, Validators.email ]),
    mob: new FormControl('', Validators.required),
    city: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required)
  });
  constructor(private cust: CustomersignupService, private router:Router) { }


  signup()
  {
    console.log(this.custform.value);
    
    this.cust.add1(this.custform.value).subscribe(success => {
      alert("Signup successful");
      this.router.navigateByUrl('/login/customer');
    },
    error => {
      alert(error);
    });
  }
  ngOnInit() {
  }

}
