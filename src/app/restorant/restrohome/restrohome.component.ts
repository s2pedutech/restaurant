import { Component, OnInit } from '@angular/core';
import {ActivatedRoute,Router} from '@angular/router';
import {RestaurentService} from '../../restaurent.service';

@Component({
  selector: 'app-restrohome',
  templateUrl: './restrohome.component.html',
  styleUrls: ['./restrohome.component.css']
})
export class RestrohomeComponent implements OnInit {

  currentId;
  menu :any=[];
  constructor(private act:ActivatedRoute, private router:Router, private res:RestaurentService) { }

  getMenu()
  {
    this.res.getMenuItem(this.currentId).subscribe(success=>
      {
        this.menu = success;
      },
      err=>{

      });
    
  }
  editMenu(m)
  {
    let id = m.id;
    this.router.navigate(['/restro/menu/'], {
      queryParams : {
        "restroid" : this.currentId,
        "id": id
      }
    });
  }
  deleteMenu()
  {

  }
  ngOnInit() {
    this.act.queryParams.subscribe(params => {
      console.log(params.id);
      this.currentId = params.id;
    });

    this.getMenu();
    
  }

  addMenu()
  {
    this.router.navigate(['/restro/menu/'], {
      queryParams : {
        "restroid" : this.currentId
      }
    });
  }



}
