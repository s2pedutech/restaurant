import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {RestaurentService} from '../../restaurent.service';
import {ActivatedRoute,Router} from '@angular/router';
@Component({
  selector: 'app-restromenu',
  templateUrl: './restromenu.component.html',
  styleUrls: ['./restromenu.component.css']
})
export class RestromenuComponent implements OnInit {

  restroid;
  itemid;
  constructor(private resser:RestaurentService, private act:ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.act.queryParams.subscribe(params => {
      this.restroid = params.restroid;
      this.itemid = params.id;
      this.getItemDetails();
    });
  }
additem=new FormGroup({
  id: new FormControl(),
  restroid: new FormControl(),
  name:new FormControl(),
  cost:new FormControl()
});

getItemDetails()
{
  this.resser.getMenuItem(this.itemid).subscribe(success => {
    let menuitem:any = success;
    if(menuitem.length == 1)
    {
      console.log(menuitem[0]);
      this.additem.patchValue(menuitem[0]);
    }
  }, error => {
    console.log(error);
  });
}
add()
{
  this.additem.controls.restroid.setValue(this.restroid);
  // call the service api here
  if(this.itemid!=null)
  {
     this.resser.editItem(this.itemid,this.restroid).subscribe(success=>{
      let menuitem:any = success;
    if(menuitem.length == 1)
    {
      console.log(menuitem[0]);
    }
    },error=>{console.log(error)})
  }
  else
  {
  this.resser.addItem(this.additem.value).subscribe(success => {
    console.log(success);
    this.router.navigate(['/restro/home'], {
      queryParams : {
        "id" : this.restroid
      }
    });
  }, error => {
    console.log(error);
  });
}
}

}
