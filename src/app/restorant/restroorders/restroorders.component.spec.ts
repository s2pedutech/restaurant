import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RestroordersComponent } from './restroorders.component';

describe('RestroordersComponent', () => {
  let component: RestroordersComponent;
  let fixture: ComponentFixture<RestroordersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RestroordersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RestroordersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
