import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RestrohomeComponent } from './restrohome/restrohome.component';
import { RestroordersComponent } from './restroorders/restroorders.component';
import { RestromenuComponent } from './restromenu/restromenu.component';
import {RouterModule,Routes} from '@angular/router';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
//import { RestomenuComponent } from '../customer/restomenu/restomenu.component';
var routes:Routes=[
  {path:'home',component:RestrohomeComponent},
  {path:'orders',component:RestroordersComponent},
  {path:'menu',component:RestromenuComponent},
  
  {path:'',redirectTo:'home',pathMatch:'full'}
];


@NgModule({
  declarations: [RestrohomeComponent, RestroordersComponent, RestromenuComponent],
  imports: [
    CommonModule,RouterModule.forChild(routes), FormsModule,ReactiveFormsModule
  ]
})
export class RestorantModule { }
