import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class RestaurentService {

  constructor(private http:HttpClient) { }

  getAll()
  {
    return this.http.get("http://localhost:3000/restaurant");
  }

  addrestaurent(r)
  {
    return this.http.post("http://localhost:3000/restaurant/add",r);
  }

  getByEmail(email)
  {
    return this.http.get("http://localhost:3000/restaurant/getByEmail?email=" + email);
  }

  getById(id)
  {
    return this.http.get("http://localhost:3000/restaurant/getById?id="+ id);
  }
  addItem(s)
  {
    return this.http.post("http://localhost:3000/menu/add",s);
  }
  editItem(id,restid)
  {
    return this.http.get("http://localhost:3000/restaurant/edit?id="+ id+"&rest_id=" +restid);
  }

  getMenuItem(id)
  {
    return this.http.get("http://localhost:3000/restaurant/getAllMenu?id=" + id);

  }
}
