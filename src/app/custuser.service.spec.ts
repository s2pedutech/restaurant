import { TestBed } from '@angular/core/testing';

import { CustuserService } from './custuser.service';

describe('CustuserService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CustuserService = TestBed.get(CustuserService);
    expect(service).toBeTruthy();
  });
});
