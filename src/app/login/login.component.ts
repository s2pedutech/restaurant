import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {RestaurentService} from '../restaurent.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginform = new FormGroup({
    email: new FormControl(),
    password: new FormControl()
  });

  constructor(private router:Router, private resSer:RestaurentService) { }

  ngOnInit() {
  }

  login()
  {
        this.resSer.getByEmail(this.loginform.value.email).subscribe(success => {
          console.log(success);
          let restro:any = success;
          if(restro.length == 1)
          {
            if(restro[0].password == this.loginform.value.password)
            {
              alert('login successful');
              this.router.navigate(['/restro/home'], {
                queryParams: {
                  "id" : restro[0].id
                }
              });
            }
            else
          {
            alert("Check pwd");
          }
          }
          else
          {
            alert("Check email");
          }
        }, error => {
          console.log(error);
          alert("Check user/pwd");
        });
  }
}
