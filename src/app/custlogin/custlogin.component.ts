import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {CustomersignupService} from '../customersignup.service';
import {Router} from '@angular/router';
import {CustuserService} from '../custuser.service';
@Component({
  selector: 'app-custlogin',
  templateUrl: './custlogin.component.html',
  styleUrls: ['./custlogin.component.css']
})
export class CustloginComponent implements OnInit {

  loginform = new FormGroup({
    email: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required)
  });
  
  constructor(private cust:CustomersignupService, private router:Router, private cuser:CustuserService) { }

  login()
  {
    console.log(this.loginform.value);
    this.cust.getByEmailID(this.loginform.value.email).subscribe(success => {
      console.log(success);
      
      let customer:any = success;
      if(customer.length == 1)
      {
      if(this.loginform.value.password == customer[0].password)
      {
        this.cuser.setCurrUser(customer[0]);
        this.router.navigateByUrl('/customer');
      }
      else{
        console.log("Login not successfule");
        this.cuser.setCurrUser(null);
        alert("Login unsuccessful");
      }
      }
      else
      {
        alert("Incorrect email/password");
        this.cuser.setCurrUser(null);
      }
    }, error => {
      console.log(error);
    });
  }
  ngOnInit() {
  }

}
