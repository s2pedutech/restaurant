import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RestosignupComponent } from './restosignup.component';

describe('RestosignupComponent', () => {
  let component: RestosignupComponent;
  let fixture: ComponentFixture<RestosignupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RestosignupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RestosignupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
