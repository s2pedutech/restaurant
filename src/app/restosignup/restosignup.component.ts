import { Component, OnInit } from '@angular/core';
import {FormGroup,FormControl,Validators} from '@angular/forms';
import {RestaurentService} from '../restaurent.service';
import {Router} from '@angular/router';
@Component({
  selector: 'app-restosignup',
  templateUrl: './restosignup.component.html',
  styleUrls: ['./restosignup.component.css']
})
export class RestosignupComponent implements OnInit {
  restroForm = new FormGroup({
    name: new FormControl(),
    email : new FormControl(),
    mobile: new FormControl(),
    city: new FormControl(),
    password: new FormControl()
  })

  constructor(private resSer:RestaurentService, private router:Router) { }

  ngOnInit() {
  }

  signup()
  {
    this.resSer.addrestaurent(this.restroForm.value).subscribe(success => {
      console.log(success);
      this.router.navigateByUrl('/restro/home');
    }, error => {
      console.log(error);
    })
  }

}
