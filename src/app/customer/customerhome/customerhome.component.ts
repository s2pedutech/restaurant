import { Component, OnInit } from '@angular/core';
import {RestaurentService} from '../../restaurent.service';
import {CustuserService} from '../../custuser.service';
import {Router} from '@angular/router';
@Component({
  selector: 'app-customerhome',
  templateUrl: './customerhome.component.html',
  styleUrls: ['./customerhome.component.css']
})
export class CustomerhomeComponent implements OnInit {

  restaurents:any = [];
  constructor(private restro:RestaurentService, private cuser:CustuserService, private router:Router) { }

  ngOnInit() {
    // check if current user is logged in

    console.log(this.cuser.getCurrentUser());
    if(this.cuser.getCurrentUser() == null)
    {
      this.router.navigateByUrl('/login/customer');
    }
    else
      this.getAllRestros();
  }

  logout()
  {
    this.cuser.setCurrUser(null);
    this.router.navigateByUrl('/login/customer');
  }
  getAllRestros()
  {
    this.restro.getAll().subscribe(success => {
      console.log(success);
      this.restaurents = success;
    }, error => {
      console.log(error);
    });
  }

}
