import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RestodetailsComponent } from './restodetails.component';

describe('RestodetailsComponent', () => {
  let component: RestodetailsComponent;
  let fixture: ComponentFixture<RestodetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RestodetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RestodetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
