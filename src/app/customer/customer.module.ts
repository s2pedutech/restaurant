import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomerhomeComponent } from './customerhome/customerhome.component';
import { RestodetailsComponent } from './restodetails/restodetails.component';
import { RestomenuComponent } from './restomenu/restomenu.component';
import {RouterModule,Routes} from '@angular/router';
var routes:Routes=[
  {path:'home',component:CustomerhomeComponent},
 {path:'restros',component:RestodetailsComponent},
 {path:'restomenu',component:RestomenuComponent},
 {path:'',redirectTo:'home',pathMatch:'full'}
]


@NgModule({
  declarations: [CustomerhomeComponent, RestodetailsComponent, RestomenuComponent],
  imports: [
    CommonModule,RouterModule.forChild(routes)
  ]
})
export class CustomerModule { }
