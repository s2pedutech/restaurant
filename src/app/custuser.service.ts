import { Injectable,Inject } from '@angular/core';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';

@Injectable({
  providedIn: 'root'
})
export class CustuserService {

  curruser:any = null;
  constructor(@Inject(LOCAL_STORAGE) private storage: StorageService) { }


  getCurrentUser()
  {
    if(this.curruser == null)
    {
      this.curruser = this.storage.get("customer");
    }
    return this.curruser;
  }

  setCurrUser(c)
  {
    this.storage.set("customer", c);
    this.curruser = c;
  }
}
