import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CustloginService {

  constructor(private http: HttpClient) { }

  login()
  {
    return this.http.get("http://localhost:3000/customer/getByEmail");
  }
}
