import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminhomeComponent } from './adminhome/adminhome.component';
import { UserComponent } from './user/user.component';
import { RestroComponent } from './restro/restro.component';
import { AdminordersComponent } from './adminorders/adminorders.component';
import {RouterModule,Routes} from '@angular/router';

var routes:Routes = [
  { path: 'home', component : AdminhomeComponent},
  { path: 'users', component: UserComponent },
  {path: 'restros', component: RestroComponent},
  {path: 'orders', component: AdminordersComponent},
  { path: '', redirectTo: 'home', pathMatch: 'full'}
];


@NgModule({
  declarations: [AdminhomeComponent, UserComponent, RestroComponent, AdminordersComponent],
  imports: [
    CommonModule, RouterModule.forChild(routes)
  ]
})
export class AdminModule { }
