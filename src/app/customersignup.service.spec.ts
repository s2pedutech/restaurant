import { TestBed } from '@angular/core/testing';

import { CustomersignupService } from './customersignup.service';

describe('CustomersignupService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CustomersignupService = TestBed.get(CustomersignupService);
    expect(service).toBeTruthy();
  });
});
