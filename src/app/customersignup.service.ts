import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CustomersignupService {

  url:string = "http://localhost:3000/customer";
  getAll()
  {
    return this.http.get(this.url);
  }

  add1(c)
  {
    return this.http.post(this.url + "/add", c);
  }

  getByEmailID(email)
  {
    let path = this.url + "/getByEmail/" + "?email=" + email;
    console.log(path);
    return this.http.get(path);
  }
  constructor(private http:HttpClient) { }
}
